#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include <cassert>

using JosephusPivots = std::vector<std::pair<int,int>>;

JosephusPivots josephusPivots(int, int);
int josephusIndex(int, int, const JosephusPivots&);

int main(int argc, char** argv)
{
    int k;
    int N;
    if (argc < 2)
    {
        std::cout << "k = "; //for example, 18
        std::cin >> k;
        std::cout << "N = "; //for example, 10000
        std::cin >> N;
    }
    else
    {
        k = atoi(argv[1]);
        N = atoi(argv[2]);
        std::cout << "k = " << k << "\n";
        std::cout << "N = " << N << "\n";
    }

    int w = std::to_string(N).size();
    std::cout << std::setw(w) << "n" << " | "
              << std::setw(w+2) << "W(n)" << "\n"
              << std::setw(w+1) << std::setfill('-') << '-' << '+'
              << std::setw(2*w+2) << "\n" << std::setfill(' ');

    auto pivots = josephusPivots(N, k);
    for (int n = 1; n <= N; ++n)
    {
        // Find W(n,k) by brute force
        std::vector<int> p(n);
        std::iota(begin(p), end(p), 1);
        for (int i = 1; p.size() > 1; i %= k)
        {
            for (auto& x : p) if (i++ % k == 0) x = 0;
            p.erase(std::remove(begin(p), end(p), 0), end(p));
        }
        // Assert W(n,k) == josephusIndex(n, k)
        assert(p[0] == josephusIndex(n, k, pivots));
        //
        if (n % 1000 == 0)
            std::cout << std::setw(w) << n << " | "
                      << std::setw(w) << p[0] << " "
                      << josephusIndex(n, k, pivots) << "\n";
    }
}


JosephusPivots josephusPivots(int N, int k)
{
    JosephusPivots p;
    p.emplace_back(1, 1);
    for (int i = 2; i < k && p.back().first <= N; ++i)
    {
        int w = (p.back().second + k) % i;
        if (w == 0) w = i;
        p.emplace_back(i, w);
    }
    while (p.back().first <= N)
    {
        int m = (p.back().first - p.back().second) / (k - 1) + 1;
        p.emplace_back(p.back().first + m, p.back().second + m*k - p.back().first - m);
    }
    return p;
}

int josephusIndex(int n, int k, const JosephusPivots& pivots)
{
    auto it = --std::upper_bound(begin(pivots), end(pivots), n,
                    [](int x, auto const& p){ return x < p.first; });
    if (it->first == n) return it->second;
    return (n - it->first) * k + it->second;
}
