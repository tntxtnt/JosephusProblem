#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include <cassert>

using JosephusPivots = std::vector<std::pair<int,int>>;

JosephusPivots josephusPivots(int, int);
int josephusIndex(int, int, const JosephusPivots&);

int main()
{
    int n, k;
    while (1)
    {
        std::cout << "n k: ";
        std::cin >> n >> k;
        if (!n) break;
        auto pivots = josephusPivots(n, k);
        std::cout << "Safe spot: " << josephusIndex(n, k, pivots) << "\n";
    }
}


JosephusPivots josephusPivots(int N, int k)
{
    JosephusPivots p;
    p.emplace_back(1, 1);
    for (int i = 2; i < k && p.back().first <= N; ++i)
    {
        int w = (p.back().second + k) % i;
        if (w == 0) w = i;
        p.emplace_back(i, w);
    }
    while (p.back().first <= N)
    {
        int m = (p.back().first - p.back().second) / (k - 1) + 1;
        p.emplace_back(p.back().first + m, p.back().second + m*k - p.back().first - m);
    }
    return p;
}

int josephusIndex(int n, int k, const JosephusPivots& pivots)
{
    auto it = --std::upper_bound(begin(pivots), end(pivots), n,
                    [](int x, auto const& p){ return x < p.first; });
    if (it->first == n) return it->second;
    return (n - it->first) * k + it->second;
}

