#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include <cassert>

std::vector<std::pair<int,int>> getJosephusPivots_complex(int n, int k)
{
    std::vector<std::pair<int,int>> p;
    p.emplace_back(1, 1);
    p.emplace_back(2, 1 + k%2);
    for (int i = 3; i < k; ++i)
    {
        int w = (k - i + p.back().second) % i;
        if (w == 0) w = i;
        p.emplace_back(i, w);
        if (p.back().first > n) break;
    }
    while (p.back().first <= n)
    {
        int id1 = p.back().first + p.back().first / (k-1);
        if (p.back().first % (k-1) == 0)
        {
            p.emplace_back(id1, p.back().second);
            continue;
        }
        int id2 = id1 + 1;

        int r1 = id1 % k;
        int r2 = id2 % k;
        if (r1 == p.back().second)
        {
            p.emplace_back(id2, (p.back().second + k - r2)%k);
            continue;
        }
        if (r2 == p.back().second)
        {
            p.emplace_back(id1, (p.back().second + k - r1)%k);
            continue;
        }

        int w1 = (p.back().second + k - r1) % k;
        //int w2 = (p.back().second + k - r2) % k;

        int s1 = 1 - r1;
        int s2 = k + 1 - r2;
        int w_1 = s1 + p.back().second - 1;
        int w_2 = s2 + p.back().second - 1;
        if (w_1 < 0)
        {
            p.emplace_back(id2, (p.back().second + k - r2)%k);
            continue;
        }
        if (w_2 < 0)
        {
            p.emplace_back(id1, (p.back().second + k - r1)%k);
            continue;
        }

        if (w1 == w_1)
        {
            p.emplace_back(id1, (p.back().second + k - r1)%k);
            continue;
        }
        else
        {
            p.emplace_back(id2, (p.back().second + k - r2)%k);
            continue;
        }
    }
    return p;
}

auto josephusPivots(int N, int k)
{
    std::vector<std::pair<int,int>> p;
    p.emplace_back(1, 1);
    for (int i = 2; i < k && p.back().first <= N; ++i)
    {
        int w = (p.back().second + k) % i;
        if (w == 0) w = i;
        p.emplace_back(i, w);
    }
    while (p.back().first <= N)
    {
        int n = p.back().first;
        int w = p.back().second;
        int n1 = n + n / (k - 1);
        if (n % (k - 1) == 0) { p.emplace_back(n1, w); continue; }
        int n2 = n1 + 1;
        int w1 = (n1 - n) * k + w;
        int w2 = (n2 - n) * k + w;
        if (w1 > n1) p.emplace_back(n1, w1 - n1);
        else         p.emplace_back(n2, w2 - n2);
    }
    return p;
}

int josephusIndex(int n, int k, const std::vector<std::pair<int,int>>& pivots)
{
    auto it = --std::upper_bound(begin(pivots), end(pivots), n,
                    [](int x, auto const& p){ return x < p.first; });
    if (it->first == n) return it->second;
    return (n - it->first) * k + it->second;
}

int main()
{
    int k = 6;
    int N = 1000;

    auto pivots = josephusPivots(N, k);
    //for (int i = 1; i < N; ++i)
    //    std::cout << i << " | " << josephusIndex(i, k, p) << "\n";
    /*for (auto& x : p) std::cout << std::setw(5) << x.first << "  "
                                << std::setw(5) << x.second << "\n";*/

    int w = std::to_string(N).size();
    std::cout << std::setw(w) << "n" << " | "
              << std::setw(w+2) << "W(n)" << "\n"
              << std::setw(w+1) << std::setfill('-') << '-' << '+'
              << std::setw(w+4) << "\n" << std::setfill(' ');
    for (int n = 1; n < N; ++n)
    {
        //if (n%24 == 0) std::cin.get();
        std::vector<int> p(n);
        std::iota(begin(p), end(p), 1);
        for (int i = 1; p.size() > 1; )
        {
            for (auto& x : p) if (i++ % k == 0) x = 0;
            auto pend = std::remove_copy(begin(p), end(p), begin(p), 0);
            p.resize(std::distance(begin(p), pend));
        }
        //if (p[0] <= k)
        {
            /*std::cout << std::setw(w) << n << " | "
                      //<< std::setw(3) << n%(k-1) << "  "
                      //<< std::setw(3) << n%k << "  "
                      << std::setw(w) << p[0] << " "
                      << josephusIndex(n, k, pivots) << "\n";*/
            assert(p[0] == josephusIndex(n, k, pivots));
            if (n % 1000 == 0) std::cout << std::setw(w) << n << "\n";
        }
    }
}
