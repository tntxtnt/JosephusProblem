## Josephus Problem Solver
- Inspired by [Daniel Erman's video on Numberphile](https://www.youtube.com/watch?v=uCsD3ZGzMgE)
- Algorithm: geometric series O(k logn) ([Vietnamese explanation](https://tritran.xyz/posts/bai-toan-josephus/))